import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';


@IonicPage()
@Component({
  selector: 'page-catalog',
  templateUrl: 'catalog.html',
})
export class CatalogPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public authProvider:AuthProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CatalogPage');
  }

  logOut():void {
    this.authProvider.logoutUser().then( () => {
      this.navCtrl.setRoot('SigninPage');
    });
  }

}
