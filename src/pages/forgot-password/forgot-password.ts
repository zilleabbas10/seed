import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, Alert, AlertController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { EmailValidator } from '../../validators/email';

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage implements OnInit {

  public resetPasswordForm:FormGroup;

  constructor(public navCtrl:NavController, public authProvider:AuthProvider, public alertCtrl:AlertController,
              public  formBuilder:FormBuilder) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotPasswordPage');
  }

  ngOnInit() {
    this.resetPasswordForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, EmailValidator.isValid])]
    });

    console.log(this.resetPasswordForm);
  }

  resetPassword():void {
    if (!this.resetPasswordForm.valid){
      console.log(`Form isn't valid yet, current value: ${this.resetPasswordForm.value}`);
    } else {
      const email:string = this.resetPasswordForm.value.email;
      this.authProvider.resetPassword(email).then( user => {
        const alert:Alert = this.alertCtrl.create({
          message: "Check your email for a password reset link",
          buttons: [{
            text: "Ok",
            role: 'cancel',
            handler: () => { this.navCtrl.pop() }
          }]
        });
        alert.present()
      }, error => {
        const errorAlert = this.alertCtrl.create({
          message: error.message,
          buttons: [{ text: "Ok", role: "cancel" }]
        });
        errorAlert.present();
      });
    }
  }


}
