import { Component, OnInit } from '@angular/core';
import { IonicPage, Loading, LoadingController, NavController, Alert, AlertController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { EmailValidator } from '../../validators/email';
import { AuthProvider } from '../../providers/auth/auth';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage implements OnInit {

  public signupForm:FormGroup;
  public loading:Loading;

  constructor(public navCtrl:NavController, public loadingCtrl:LoadingController, public alertCtrl:AlertController,
              public authProvider:AuthProvider, public formBuilder:FormBuilder) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  ngOnInit() {
    this.signupForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      customer: ['', Validators.compose([Validators.required])]
    });

    console.log(this.signupForm);
  }

  signupUser():void {
    if(!this.signupForm.valid){
      console.log(`Need to complete the form, current value: ${this.signupForm.value}`);
    } else {
      const email:string = this.signupForm.value.email;
      const password:string = this.signupForm.value.password;
      const customer:string = this.signupForm.value.customer;

      this.authProvider.signupUser(email, password, customer).then( user => {
        this.loading.dismiss().then( () => {
          if(customer == 'buyer'){
            this.navCtrl.setRoot('CatalogPage');
          }else{
            this.navCtrl.setRoot('ProfilePage');
          }
        });
      }, error => {
        this.loading.dismiss().then( () => {
          const alert:Alert = this.alertCtrl.create({
            message: error.message,
            buttons: [{ text: "Ok", role: "cancel" }]
          });
          alert.present();
        });
      });
      this.loading = this.loadingCtrl.create();
      this.loading.present();
    }
  }

}
