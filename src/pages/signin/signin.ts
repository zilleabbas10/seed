import { Component, OnInit } from '@angular/core';
import { IonicPage, Loading, LoadingController, NavController, Alert, AlertController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { EmailValidator } from '../../validators/email';
import { AuthProvider } from '../../providers/auth/auth';

import firebase from 'firebase';


@IonicPage()
@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html',
})
export class SigninPage implements OnInit {

  public loginForm:FormGroup;
  public loading:Loading;
  public userProfile:firebase.database.Reference;
  public customer:string;

  constructor(public navCtrl:NavController, public loadingCtrl:LoadingController, public alertCtrl:AlertController,
              public authProvider:AuthProvider, public formBuilder:FormBuilder) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SigninPage');
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
    });

    console.log(this.loginForm);
  }


  loginUser():void {
    if(!this.loginForm.valid){
      console.log(`Form isn't valid yet, current value: ${this.loginForm.value}`);
    } else {
      const email = this.loginForm.value.email;
      const password = this.loginForm.value.password;

      this.authProvider.loginUser(email, password).then( authData => {
        this.loading.dismiss().then( () => {
          this.userProfile = firebase.database().ref(`/userProfile/${authData.uid}`);
          this.userProfile.on('value', userProfileSnapshot => {
            this.customer = userProfileSnapshot.val().customer;
            console.log(this.customer);
            if(this.customer == 'buyer'){
              this.navCtrl.setRoot('CatalogPage');
            }else{
              this.navCtrl.setRoot('ProfilePage');
            }
          })
          console.log(authData);
        });
      }, error => {
        this.loading.dismiss().then( () => {
          const alert:Alert = this.alertCtrl.create({
            message: error.message,
            buttons: [{ text: "Ok", role: 'cancel'}]
          });
          alert.present()
        });
      });
      this.loading = this.loadingCtrl.create();
      this.loading.present()
    }
  }

  goToSignup():void {
    this.navCtrl.push('SignupPage');
  }

  goToResetPassword():void {
    this.navCtrl.push('ForgotPasswordPage');
  }

}
