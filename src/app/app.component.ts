import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import firebase from 'firebase';

import { HomePage } from '../pages/home/home';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  public userProfile:firebase.database.Reference;
  public customer:string;
  rootPage:any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {

    firebase.initializeApp({
      apiKey: "AIzaSyBcU9-oX8pEPCo1DKhXPCiQSNbn7y9fAuY",
      authDomain: "mathieu-seed.firebaseapp.com",
      databaseURL: "https://mathieu-seed.firebaseio.com",
      projectId: "mathieu-seed",
      storageBucket: "mathieu-seed.appspot.com",
      messagingSenderId: "1055310233200"
    });

    const unsubscribe = firebase.auth().onAuthStateChanged( user => {
      if(!user){
        this.rootPage = 'SigninPage';
        unsubscribe();
      } else {
        this.userProfile = firebase.database().ref(`/userProfile/${user.uid}`);
        this.userProfile.on('value', userProfileSnapshot => {
          this.customer = userProfileSnapshot.val().customer;
          console.log(this.customer);
          if(this.customer == 'buyer'){
            this.rootPage = 'CatalogPage';
            unsubscribe();
          }else{
            this.rootPage = 'ProfilePage';
            unsubscribe();
          }
        })
      }
    });


    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

